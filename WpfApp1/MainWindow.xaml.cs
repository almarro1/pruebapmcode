﻿using PMCode.algorithm.PALIA.core;
using PMCode.CSVFactory;
using PMCode.filter;
using PMCode.layout;
using PMCode.log;
using PMCode.models.tpa;
using PMCode.models.tpa.attributes;
using PMCode.view.tpa;
using PMCode.view.tpa.render;
using Sabien.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WpfApp1
{
    /// <summary>
    /// Lógica de interacción para MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        // visor grafico de TPAs
        TPAViewer view = new TPAViewer();

        public MainWindow()
        {
            InitializeComponent();
            gBase.Children.Add(view); // añadimos el visor de TPA a la parte grafica
        }

        private void bexp1_Click(object sender, RoutedEventArgs e)
        {// En este ejemplo Aplicamos Discovery sobre un CSV con PALIA


            string CSV = "CElaFe.csv";
            // Esto si queremos que salga un dialogo
            //CSV =Sabien.Windows.Utils.FileHelper.OpenFileDialogo("CSV FIles (*.csv)|*.csv");

            // leemos el log usando un factory de CSV
            ProcessMiningLog pml = PMLogReader.FromCSV(CSV);
            // Aplicamos filtro de muestras solo coge las que van de x a y
            pml = new SplitLogFilter(0, 100).ProcessLog(pml);
            pml = new SequencializeActivities2().ProcessLog(pml);

            // Aplicamos PALIA al LOG
            PALIAAlgorithm pa = new PALIAAlgorithm();
            TPATemplate tpa =   pa.InfiereEsquema(pml);

            // aplicamos un skin al TPA que hemos aprendido en un TPA para que tipe la informacion grafica de posicion
            TPATemplateGUI tpagui = new TPATemplateGUI(tpa);
            // aplicamos el algoritmo de spring forces para que quede distribuya los nodos por la pantalla
            tpagui = new SpringForcesLayout(500, 10, 10).CalculateLayout(tpagui);
            //aplicamos un skin de inferred tpa para que obtener las estadisticas
            InferredTPA inf = new InferredTPA(tpagui.data);
            inf.CalculaStats();

            view.ShowTPATemplate(inf);
            // coloreamos los nodos por duracion de actividad
            NodeColorRenderHandler<ActivityDurationAttribute> nc = new NodeColorRenderHandler<ActivityDurationAttribute>()
            {
                SatHigh = 1,
                SatLow = 0
            };
            nc.Render(view.tve);

            // Coloreamos las transiciones por numero de ejecucion
            TransitionColorRenderHandler<ExecutionsNumberAttribute> tc = new TransitionColorRenderHandler<ExecutionsNumberAttribute>()
            {
                SatHigh = 1,
                SatLow = 0
            };
            tc.Render(view.tve);

        }

       
    }
}
