﻿using PMCode.log;
using Sabien.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PMCode.CSVFactory
{
    public class PMLogReader
    {
        public const string SEPARADOR = "@";
        public const string CABID = "ID";
        public const string CABNAME = "NAME";
        public const string CABACTIVITY = "ACTIVITY";
        public const string CABSTART = "START";
        public const string CABEND = "END";
        public const string CABSAMPLE = "SAMPLE";
        public const string CABRESULT = "RESULT";

        public static ProcessMiningLog FromCSV(string csv)
        {
            ProcessMiningLog res = new ProcessMiningLog();
            // El CSVIterator Es un componente que itera por csv creando maps(en forma de diccionario de cada una de las variables
            // utiliza la primera fila como los campos de cabecera de cada una de las filas, y asi podemos leer de forma tipada el
            //resto de las filas
            CSVIterator csvi = new CSVIterator();


            foreach (Dictionary<string, string> l in csvi.IterateTypedFile(csv))
            {// Para cada una de las filas del CSV devuelve un diccionario con la información de cada Fila
                List<string> ID = new List<string>(); // la lista de ID para crear el ID Unico
                List<string> ACTIVITINAME = new List<string>(); // lista de Actividades para crear el nombre unico de la actividad
                Dictionary<string, string> SAMPLEDATA = new Dictionary<string, string>(); // diccionario de metadatos de la muestra
                ProcessMiningActivity pma = new ProcessMiningActivity(); // Evento(actividad) que se va a crear por cada linea iterada en el csv

                foreach (string k in l.Keys)
                {
                    // Separamos las lineas del CSV por el sepadador asociado 
                    // En la cabecera se espera Nombre@TIPO
                    string[] pair = k.Split(new string[] { SEPARADOR }, StringSplitOptions.RemoveEmptyEntries); 
                    if (pair.Length > 1)
                    {
                        string campo = pair[0]; // la parte izquierda de la cabecera es el nombre de la propiedad
                        string tipo = pair[1];// la parte derecha de la cabecera en el tipo de campo
                        string valor = l[k]; // el valor asociado a la cabecera en la linea actual
                        // analizamos todos los campos
                        switch (pair[1])
                        {
                            case CABID: // este tipo dice que es un identificador
                                ID.Add(valor);
                                break;
                            case CABNAME: // este tipo dice que es un nombre y por lo tanto es el que debe aparecer en el nombre de la actividad
                                ACTIVITINAME.Add(valor);
                                pma.MetaData.Set(campo, valor);
                                break;
                            case CABACTIVITY: // Es una variable de Actividad
                                pma.MetaData.Set(campo, valor);
                                break;
                            case CABSTART: // Es el timestamp de inicio
                                pma.Start = DateTime.Parse(valor);
                                break;
                            case CABEND: // es el timestamp de Fin
                                pma.End = DateTime.Parse(valor);
                                break;
                            case CABSAMPLE: // Es una variable de la muestra, es decir del mismo usuario no solo de esta actividad
                                SAMPLEDATA[campo] = valor;
                                break;
                            case CABRESULT: // es un resultado de la actividad actual
                                if (pma.Results == null) { pma.Results = new List<string>(); }
                                pma.Results.Add(valor);
                                break;
                                        
                        }
                    }

                }
                //Calculamos el ID de la muestra concatenando los ID seleccionados
                pma.SampleId = string.Join("_", ID);
                //Calculamos el Nmbre de la Actividad concatenando los Nombres seleccionados
                pma.ActivityName = string.Join("_", ACTIVITINAME);
                // insertamos el evento en el log ordenado por su start timestamp
                res.InsertEvent(pma);
                // recupera la sample del log y modifica su sample dato. Ojo queda vigente solo la ultima modificadion
                ProcessMiningSample pms = res.Samples[pma.SampleId];
                foreach (string k in SAMPLEDATA.Keys)
                {
                    if (!pms.MetaData.Contains(k))
                    {// se escoge el primer valor
                        pms.MetaData.Set(k, SAMPLEDATA[k]);
                    }
                }

                
               
            }

            return res;
        }
    }
}
